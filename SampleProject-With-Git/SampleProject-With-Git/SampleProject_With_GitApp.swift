//
//  SampleProject_With_GitApp.swift
//  SampleProject-With-Git
//
//  Created by sylvester on 9/11/23.
//

import SwiftUI

@main
struct SampleProject_With_GitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
